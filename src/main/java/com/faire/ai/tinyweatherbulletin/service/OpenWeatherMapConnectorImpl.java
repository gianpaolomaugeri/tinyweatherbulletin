package com.faire.ai.tinyweatherbulletin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.faire.ai.tinyweatherbulletin.model.Root;

@Service
public class OpenWeatherMapConnectorImpl implements OpenWeatherMapConnector {
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public Root openWeatherMapConnector(String city){
		String uri = "http://api.openweathermap.org/data/2.5/forecast?q="+city+"&units=metric&lang=en,uk&APPID=3e07c4a6aab0b543d438c900239d71e2";
		return this.restTemplate.getForObject( uri, Root.class);
	}
	
}
