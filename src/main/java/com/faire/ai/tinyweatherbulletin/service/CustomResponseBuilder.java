package com.faire.ai.tinyweatherbulletin.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.faire.ai.tinyweatherbulletin.model.Day;
import com.faire.ai.tinyweatherbulletin.model.ListObject;
import com.faire.ai.tinyweatherbulletin.model.Response;
import com.faire.ai.tinyweatherbulletin.util.Average;
import com.faire.ai.tinyweatherbulletin.util.ListFilter;

@Service
public class CustomResponseBuilder {
	
	private enum Days{
		FIRST (1) ,
		SECOND (2),
		THIRD(3);

		private int number;
		
		Days(int number) {
			this.number = number;
		}

		public int getNumber(){
			return number;
		}
	}
	
	public Response buildResponce(List<ListObject> list, LocalDate currentDate, String entryTime, String exitTime) {
		
		List<ListObject> filterdList = list
				.stream()
				.filter(Objects::nonNull)
				.filter(listObject -> ListFilter.checkNext3DaysInterval(listObject.getDt_txt(),currentDate))
				.collect(Collectors.toList());
		
		List<Day> daylyList = new ArrayList<>();
		
		EnumSet.allOf(Days.class)
		  .forEach(dayNumber -> {
			  
			  LocalDate tempLocalDate = currentDate.plusDays(dayNumber.getNumber());
			  List<ListObject> listObject = ListFilter.filterListObjectByLocalDate (filterdList,tempLocalDate);
			  
			  List<ListObject> listObjectDuringWorkingHours = ListFilter.filterListObjectInWorkingHours(listObject,entryTime, exitTime);
			  List<ListObject> listObjectOutsideWorkingHours = ListFilter.filterListObjectOutWorkingHours(listObject,entryTime, exitTime);
			  
			  double averageTemperatureDuringWorkingHours = Average.averageTemperature(listObjectDuringWorkingHours);
			  double averageTemperatureOutsideWorkingHours = Average.averageTemperature(listObjectOutsideWorkingHours);
			  
			  
			  int averageHumidityDuringWorkingHours = Average.averageHumidity(listObjectDuringWorkingHours);
			  int averageHumidityOutsideWorkingHours = Average.averageHumidity(listObjectOutsideWorkingHours);
			  
			  Day day = new Day();
			  day.setDate(tempLocalDate);
			  day.setAverageHumidityInWorkingHours(averageHumidityDuringWorkingHours);
			  day.setAverageHumidityOutWorkingHours(averageHumidityOutsideWorkingHours);
			  day.setAverageTemperatureInWorkingHours(averageTemperatureDuringWorkingHours);
			  day.setAverageTemperatureOutWorkingHours(averageTemperatureOutsideWorkingHours);
			  
			  daylyList.add(day);
		  });
		
		Response response = new Response();
		response.setDayList(daylyList);
		return response;
	}
}
