package com.faire.ai.tinyweatherbulletin.service;

import com.faire.ai.tinyweatherbulletin.model.Root;

public interface OpenWeatherMapConnector {

	Root openWeatherMapConnector(String city);


}