package com.faire.ai.tinyweatherbulletin.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.faire.ai.tinyweatherbulletin.model.ListObject;
import com.faire.ai.tinyweatherbulletin.model.Response;




@Service
public class BusinessLogic {
	@Autowired
	OpenWeatherMapConnector openWeatherMapConnector;
	
	@Autowired
	CustomResponseBuilder responseBuilder;
	
	public Response run(String city, String entryTime,String exitTime){
		
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		List<ListObject> list = openWeatherMapConnector.openWeatherMapConnector(city).getList();
		return responseBuilder.buildResponce(list, currentDate, entryTime, exitTime);
		
	}
	
	
	
	
	

}



