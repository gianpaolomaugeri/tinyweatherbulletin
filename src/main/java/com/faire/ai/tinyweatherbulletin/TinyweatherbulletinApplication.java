package com.faire.ai.tinyweatherbulletin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TinyweatherbulletinApplication {

	public static void main(String[] args) {
		SpringApplication.run(TinyweatherbulletinApplication.class, args);
	}

}
