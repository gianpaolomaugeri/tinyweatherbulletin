package com.faire.ai.tinyweatherbulletin.model;

import lombok.Getter;
import lombok.Setter;


public class Coord{
    private @Getter @Setter double lat;
    private @Getter @Setter double lon;
}

