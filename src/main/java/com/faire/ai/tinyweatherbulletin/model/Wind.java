package com.faire.ai.tinyweatherbulletin.model;

import lombok.Getter;
import lombok.Setter;

public class Wind{
    private @Getter @Setter double speed;
    private @Getter @Setter int deg;
}