package com.faire.ai.tinyweatherbulletin.model;

import lombok.Getter;
import lombok.Setter;

public class Weather{
    private @Getter @Setter int id;
    private @Getter @Setter String main;
    private @Getter @Setter String description;
    private @Getter @Setter String icon;
}

