package com.faire.ai.tinyweatherbulletin.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class Response {
	private @Getter @Setter List<Day> dayList;
}
