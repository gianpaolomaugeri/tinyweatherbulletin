package com.faire.ai.tinyweatherbulletin.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;


public class Root{
    private @Getter @Setter String cod;
    private @Getter @Setter int message;
    private @Getter @Setter int cnt;
    private @Getter @Setter List<ListObject> list;
    private @Getter @Setter City city;
}