package com.faire.ai.tinyweatherbulletin.model;

import lombok.Getter;
import lombok.Setter;

public class Sys{
	private @Getter @Setter String pod;
}