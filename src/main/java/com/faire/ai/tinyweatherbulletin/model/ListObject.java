package com.faire.ai.tinyweatherbulletin.model;
import java.util.List;

import lombok.Getter;
import lombok.Setter;


public class ListObject{
    private @Getter @Setter long dt;
    private @Getter @Setter Main main;
    private @Getter @Setter List<Weather> weather;
    private @Getter @Setter Clouds clouds;
    private @Getter @Setter Wind wind;
    private @Getter @Setter int visibility;
    private @Getter @Setter double pop;
    private @Getter @Setter Sys sys;
    private @Getter @Setter String dt_txt;
}