package com.faire.ai.tinyweatherbulletin.model;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

public class Day {
	private @Getter @Setter LocalDate date;
	private @Getter @Setter int averageHumidityOutWorkingHours;
	private @Getter @Setter int averageHumidityInWorkingHours;
	private @Getter @Setter double averageTemperatureOutWorkingHours;
	private @Getter @Setter double averageTemperatureInWorkingHours;
}
