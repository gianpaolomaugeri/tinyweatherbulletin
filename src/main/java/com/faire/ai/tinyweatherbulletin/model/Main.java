package com.faire.ai.tinyweatherbulletin.model;

import lombok.Getter;
import lombok.Setter;

public class Main{
    private @Getter @Setter double temp;
    private @Getter @Setter double feels_like;
    private @Getter @Setter double temp_min;
    private @Getter @Setter double temp_max;
    private @Getter @Setter int pressure;
    private @Getter @Setter int sea_level;
    private @Getter @Setter int grnd_level;
    private @Getter @Setter int humidity;
    private @Getter @Setter double temp_kf;
}