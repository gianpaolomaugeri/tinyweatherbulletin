package com.faire.ai.tinyweatherbulletin.model;

import lombok.Getter;
import lombok.Setter;


public class City{
    private @Getter @Setter int id;
    private @Getter @Setter String name;
    private @Getter @Setter Coord coord;
    private @Getter @Setter String country;
    private @Getter @Setter int population;
    private @Getter @Setter int timezone;
    private @Getter @Setter int sunrise;
    private @Getter @Setter int sunset;
}