package com.faire.ai.tinyweatherbulletin.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.faire.ai.tinyweatherbulletin.model.ListObject;
import com.faire.ai.tinyweatherbulletin.model.Root;
import com.faire.ai.tinyweatherbulletin.service.BusinessLogic;
import com.faire.ai.tinyweatherbulletin.service.OpenWeatherMapConnector;
import com.faire.ai.tinyweatherbulletin.service.CustomResponseBuilder;

import lombok.Getter;
import lombok.Setter;

@RestController
@RequestMapping("/api")
public class ForecastCityController {
	
		
	
	
	@Autowired
	BusinessLogic businessLogic;
	
	@GetMapping
	public String hello() {
		return "hello!";
	}
	
	@GetMapping("/saluta")
	public String hello2() {
		return "Ciao!";
	}
	
	@GetMapping("/responseEntity")
	public ResponseEntity hello3(){
		return new ResponseEntity<>("Ciao!", HttpStatus.CREATED);
	}
	
	@GetMapping("/weatherbulletin/{city}")
	public ResponseEntity weatherbulletin(@PathVariable("city") String city){
		return new ResponseEntity<>("Ciao " + city, HttpStatus.CREATED);
	}
	
	
	
	@GetMapping("/weatherbulletin/test/{city}/{entryTime}/{exitTime}")
	public ResponseEntity weatherbulletintest(@PathVariable("city") String city, @PathVariable("entryTime") String entryTime, @PathVariable("exitTime") String exitTime){
	
		//String entryTime = "09:00";
		//String exitTime = "18:00";
		
		return new ResponseEntity<> (businessLogic.run(city,entryTime, exitTime), HttpStatus.CREATED);
	
	}
	
	
	
	
	
	
	
	
	
}
