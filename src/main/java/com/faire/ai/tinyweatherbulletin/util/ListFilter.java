package com.faire.ai.tinyweatherbulletin.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import com.faire.ai.tinyweatherbulletin.model.ListObject;

public interface ListFilter {
	
	public static boolean checkNext3DaysInterval(String date, LocalDate currentDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDate localDate = LocalDateTime.parse(date, formatter).toLocalDate();
		LocalDate currentDatePlus4Days = currentDate.plusDays(4);
		return localDate.isBefore(currentDatePlus4Days) && localDate.isAfter(currentDate) ;
		
	}
	
	public static boolean isListObjectInDateOfBelonging(String date, LocalDate dateOfBelonging) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDate localDate = LocalDateTime.parse(date, formatter).toLocalDate();
		return localDate.isEqual(dateOfBelonging);
		
	}
	
	public static List<ListObject> filterListObjectByLocalDate(List<ListObject> list, LocalDate currentDate){
		return list
				.stream()
				.filter(listObject-> isListObjectInDateOfBelonging(listObject.getDt_txt(), currentDate))
				.collect(Collectors.toList());
	}
	
	public static boolean isListObjectInWorkingHours(String date, String entryTime, String exitTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalTime localTime = LocalDateTime.parse(date, formatter).toLocalTime();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		LocalTime entryTimeLocalTime = LocalTime.parse(entryTime,dtf);
		LocalTime exitTimeLocalTime = LocalTime.parse(exitTime,dtf);
		return (localTime.equals(entryTimeLocalTime) || localTime.isAfter(entryTimeLocalTime))
				&& (localTime.equals(exitTimeLocalTime) || localTime.isBefore(exitTimeLocalTime));
		
	}
	
	public static List<ListObject> filterListObjectInWorkingHours(List<ListObject> list, String entryTime, String exitTime){
		return list
		.stream()
		.filter(listObject -> isListObjectInWorkingHours(listObject.getDt_txt(),entryTime, exitTime))
		.collect(Collectors.toList());
	}
	
	public static boolean isListObjectOutWorkingHours(String date, String entryTime, String exitTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalTime localTime = LocalDateTime.parse(date, formatter).toLocalTime();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		LocalTime entryTimeLocalTime = LocalTime.parse(entryTime,dtf);
		LocalTime exitTimeLocalTime = LocalTime.parse(exitTime,dtf);
		return localTime.isBefore(entryTimeLocalTime) ||  localTime.isAfter(exitTimeLocalTime);
	}
	
	public static List<ListObject> filterListObjectOutWorkingHours(List<ListObject> list, String entryTime, String exitTime){
		return list
				.stream()
				.filter(listObject -> isListObjectOutWorkingHours(listObject.getDt_txt(),entryTime, exitTime))
				.collect(Collectors.toList());
	}


}