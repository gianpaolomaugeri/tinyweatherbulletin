package com.faire.ai.tinyweatherbulletin.util;

import java.util.List;
import java.util.NoSuchElementException;

import com.faire.ai.tinyweatherbulletin.model.ListObject;
import com.faire.ai.tinyweatherbulletin.model.Main;

public interface Average {

	public static double averageTemperature(List<ListObject> list) {
		try {
			return list.stream().mapToDouble(listObject -> {
				Main main = listObject.getMain();
				return (main.getTemp_max() + main.getTemp_min()) / 2;
			}).average().getAsDouble();
		} catch (NoSuchElementException e) {
			return 0;
		}
	}

	public static int averageHumidity(List<ListObject> list) {
		return (int) list.stream().mapToDouble(listObject -> listObject.getMain().getHumidity()).average()
				.getAsDouble();
	}

}