package com.faire.ai.tinyweatherbulletin.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import com.faire.ai.tinyweatherbulletin.model.ListObject;
import com.faire.ai.tinyweatherbulletin.util.Average;



@SpringBootTest
public class TestAverage {
	
	//@InjectMocks
	//private Average average;
	
	@Test
	public void averageTemperatureTest() {
		List<ListObject> list = new ArrayList<ListObject>();
		assertEquals(0, Average.averageTemperature(list));
	}
	
	@Test
	public void averageHumidityTest() {
		List<ListObject> list = new ArrayList<ListObject>();
		assertThrows(NoSuchElementException.class, ()-> Average.averageHumidity(list));
		
	}

}
