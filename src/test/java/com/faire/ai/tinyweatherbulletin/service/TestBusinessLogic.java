package com.faire.ai.tinyweatherbulletin.service;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.faire.ai.tinyweatherbulletin.model.ListObject;
import com.faire.ai.tinyweatherbulletin.model.Response;
import com.faire.ai.tinyweatherbulletin.model.Root;
import com.faire.ai.tinyweatherbulletin.util.Average;

@SpringBootTest
public class TestBusinessLogic {
	
	@Mock
	private OpenWeatherMapConnector openWeatherMapConnector;
	
	@InjectMocks
	private BusinessLogic businessLogic;
	
	//TODO
	@Test
	public void runTest() {
		Root root = null;
		Mockito.when(openWeatherMapConnector.openWeatherMapConnector(Mockito.anyString())).thenReturn(root);
		//Response res = businessLogic.run("","","");
		
	}
	
	@Test
	public void averageHumidityTest() {
		List<ListObject> list = new ArrayList<ListObject>();
		assertThrows(NoSuchElementException.class, ()-> Average.averageHumidity(list));
		
	}

}
